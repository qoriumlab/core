Qorium version 1.0.0.0
==========================

Release is now available from:

  <https://www.qorium.org/downloads/#wallets>

This is a new minor version release, bringing various bugfixes and other
improvements.

Please report bugs using the issue tracker at github:

  <https://gitlab.com/qoriumlab/core/issues>

