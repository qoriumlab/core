![Alt text](doc/qorium.png)



#### Introducing Qorium

Qorium [QRM] is a new digital currency that enables anonymous, instant
payments to anyone, anywhere in the world. Qorium uses peer-to-peer technology
to operate with no central authority: managing transactions and issuing money
are carried out collectively by the network. 

__________________________________________________________________________
#### Specifications

###### Version: 1.0.0.0
###### Max Block Size: 5 MB
###### Proof-of-Work Algorithm: QRM
###### Block Time: ~60 seconds
###### Coin Maturity: 200 blocks
###### Difficulty Retargeting: D106 Algorithm
###### Maximum Coin Supply: 30 Million 
###### Project Funding: 2.5% Premine
###### Masternodes: Enabled
###### Masternodes Collateral: 4000 QRM
###### Masternode Reward: 40% of block reward
###### P2P Port = 29555
###### RPC Port = 29550
###### Masternodes = 29555
__________________________________________________________________________


#### Wallet Mining

- [Linux Guide](https://qorium.tech/kb/linux-mining-wallet/)
- [Windows Guide](https://qorium.tech/kb/windows-mining-wallet/)

#### CPU Mining

- [Linux Guide](https://qorium.tech/kb/linux-mining-cpu/)
- [Windows Guide](https://qorium.tech/kb/windows-mining-cpu/)

#### GPU Mining

- [Linux Guide](https://qorium.tech/kb/linux-mining-gpu/)
- [Windows Guide](https://qorium.tech/kb/windows-mining-gpu/)

#### POW Algorithm: QRM

The QRM algorithm combines six secure hashing algorithms (SHABAL + CUBEHASH + KECCAK + SIMD + ECHO + LUFFA) which results in a efficient hashing solution on both CPU and GPU mining hardware.

#### Difficulty Retargeting Algorithm (D106 - Amaury Sechet)

Recent public discussions on how to address Bitcoin Cash's hash rate oscillation has resulted in an excellent approach that addresses many weaknesses of various other algorithms.  A well developed analsysis was presented by Bitcoin Cash's developer Amaury Sechet and supported by Zawy12 via thorough analysis, with simulated results that demonstate the algorthim's effectiveness.  The proposed implementation was modified specific to Qorium blockchain.

More information can be found here:

- [Bitcoin Mailing List Discussion](https://lists.linuxfoundation.org/pipermail/bitcoin-ml/2017-August/000136.html)
- [Bitcoin News - Bitcoin Cash Hard Fork](https://news.bitcoin.com/bitcoin-cash-hard-fork-plans-updated-new-difficulty-adjustment-algorithm-chosen/)


#### Masternodes

Masternodes will be supported with a collateral requirement of **4000 QRM**.

Qorium doesn't support masternodes in the local wallet, they must be hosted remotely.

**The collateral requirement is a dynamic target value and will be updated for future
releases based on the market price of Qorium and return on investment relative to
its peer group.**


#### Building

Please use the included build system when building as Qroium uses newer dependencies compared to other coins.


#### Acknowledgements

Credit goes to Bitcoin Core and Dash for providing a basic platform for Qorium to enhance and develop, in concert with a shared desire to support the adoption of a decentralised digital currency future for the masses.


#### License

Qorium is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

#### Development Process

The `master` branch is meant to be stable. Development is normally done in separate branches.
[Tags](https://gitlab.com/qoriumlab/core/tags) are created to indicate new official,
stable release versions of Qorium.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

#### Testing

Testing and code review is the bottleneck for development; we get more pull
requests than we can review and test on short notice. Please be patient and help out by testing
other people's pull requests, and remember this is a security-critical project where any mistake might cost people
lots of money.

#### Automated Testing

Developers are strongly encouraged to write [unit tests](/doc/unit-tests.md) for new code, and to
submit new unit tests for old code. Unit tests can be compiled and run
(assuming they weren't disabled in configure) with: `make check`

There are also [regression and integration tests](/qa) of the RPC interface, written
in Python, that are run automatically on the build server.
These tests can be run (if the [test dependencies](/qa) are installed) with: `qa/pull-tester/rpc-tests.py`

The Travis CI system makes sure that every pull request is built for Windows, Linux, and OS X, and that unit/sanity tests are run automatically.

#### Manual Quality Assurance (QA) Testing

Changes should be tested by somebody other than the developer who wrote the
code. This is especially important for large or high-risk changes. It is useful
to add a test plan to the pull request description if testing the changes is
not straightforward.

#### Translations

Changes to translations as well as new translations can be submitted to
[Qorium's Transifex page](https://www.transifex.com/projects/p/qorium/).

Translations are periodically pulled from Transifex and merged into the git repository. See the
[translation process](doc/translation_process.md) for details on how this works.

**Important**: We do not accept translation changes as GitHub pull requests because the next
pull from Transifex would automatically overwrite them again.

Translators should also follow the [forum](https://www.qorium.org/forum/topic/qorium-worldwide-collaboration.88/).
