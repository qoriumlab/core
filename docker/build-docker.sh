#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/..

DOCKER_IMAGE=${DOCKER_IMAGE:-qoriumpay/qoriumd-develop}
DOCKER_TAG=${DOCKER_TAG:-latest}

BUILD_DIR=${BUILD_DIR:-.}

rm docker/bin/*
mkdir docker/bin
cp $BUILD_DIR/src/qoriumd docker/bin/
cp $BUILD_DIR/src/qorium-cli docker/bin/
cp $BUILD_DIR/src/qorium-tx docker/bin/
strip docker/bin/qoriumd
strip docker/bin/qorium-cli
strip docker/bin/qorium-tx

docker build --pull -t $DOCKER_IMAGE:$DOCKER_TAG -f docker/Dockerfile docker
