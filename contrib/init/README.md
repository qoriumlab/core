Sample configuration files for:

SystemD: qoriumd.service
Upstart: qoriumd.conf
OpenRC:  qoriumd.openrc
         qoriumd.openrcconf
CentOS:  qoriumd.init
OS X:    org.qorium.qoriumd.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
